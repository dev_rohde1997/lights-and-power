#include <FastLED.h>
// Number of RGB LEDs in the strand
#define NUM_LEDS 60

// Define the array of leds
CRGB leds[NUM_LEDS];
// Arduino pin used for Data
#define PIN 7

void setLed(int num, int r, int g, int b, int distance) {
    num = calcPos(num, distance);
    if (num > NUM_LEDS || num < 0) {
      return;
    }
    leds[num].r = r; 
    leds[num].g = g; 
    leds[num].b = b; 
}

void setColorWheel() {
  memset(leds, 0, NUM_LEDS * 3);
  for (int i = 0; i < NUM_LEDS; i++) {
    setColorWheelLed(i);
  }
  FastLED.show();
  delay(25);
}

void setColorWheelLed(int num) {
  int red = 0;
  int blue = 0;
  int green = 0;
  if (num < 20) {
    red = 255 - (14 * num);
    blue = 0;
    green = 14 * num;
  } else if (num < 40) {
    red = 0;
    blue = 14 * (num - 20);
    green = 255 - (14 * (num - 20));
  } else if (num < 60) {
    red = 15 * (num - 40);
    blue = 255 - (14 * (num - 40));;
    green = 0;

  }

  setColor(num, red, green, blue);
}

void setColor(int num, int r, int g, int b) {
    if (num > NUM_LEDS || num < 0) {
      return;
    }
    if (r > 255) r = 255;
    if (b > 255) b = 255;
    if (g > 255) g = 255;

    if (r < 0) r = 0;
    if (b < 0) b = 0;
    if (g < 0) g = 0;
    
    leds[num].r = r; 
    leds[num].g = g; 
    leds[num].b = b; 
}

int calcPos(int i, int distance) {
    int j = i + distance ;
    if (j >= NUM_LEDS) {
      j = j % NUM_LEDS;
    }
    return j;
}

void setup()
{
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, PIN>(leds, NUM_LEDS);
}


void loading() {

  const int distance = NUM_LEDS / 2;
  // one at a time
  for(int i = 0 ; i < NUM_LEDS; i++ ) {
    memset(leds, 0, NUM_LEDS * 3);

    //1
    setLed(i, 255, 0, 0, 0);
    setLed(i+1, 255, 45, 0, 0);
    setLed(i+2, 255, 150, 0, 0);
    setLed(i+3, 150, 255, 0, 0);
    setLed(i+4, 0, 255, 0, 0);
    setLed(i+5, 0, 255, 150, 0);
    setLed(i+6, 0, 0, 255, 0);
    setLed(i+7, 255, 0, 255, 0);

    //Rückwarts
    
    int j = i - NUM_LEDS;
    if (j < 0) {
      j = j * -1;
    }

    setLed(i, 255, 0, 0, 0);
    setLed(i-1, 255, 45, 0, 0);
    setLed(i-2, 255, 150, 0, 0);
    setLed(i-3, 150, 255, 0, 0);
    setLed(i-4, 0, 255, 0, 0);
    setLed(i-5, 0, 255, 150, 0);
    setLed(i-6, 0, 0, 255, 0);
    setLed(i-7, 255, 0, 255, 0);

    

    setLed(i, 255, 0, 0, distance);
    setLed(i-1, 255, 45, 0, distance);
    setLed(i-2, 255, 150, 0, distance);
    setLed(i-3, 150, 255, 0, distance);
    setLed(i-4, 0, 255, 0, distance);
    setLed(i-5, 0, 255, 150, distance);
    setLed(i-6, 0, 0, 255, distance);
    setLed(i-7, 255, 0, 255, distance);

    setLed(i, 255, 0, 0, distance);
    setLed(i+1, 255, 45, 0, distance);
    setLed(i+2, 255, 150, 0, distance);
    setLed(i+3, 150, 255, 0, distance);
    setLed(i+4, 0, 255, 0, distance);
    setLed(i+5, 0, 255, 150, distance);
    setLed(i+6, 0, 0, 255, distance);
    setLed(i+7, 255, 0, 255, distance);
    
    FastLED.show();
    delay(30);
  }
}
void loop() {
  loading();

  //setColorWheel();
}
