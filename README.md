# Lights and Power

Lights and Power is a simple project for an arduino. 

The project aims to control the LED stip when the messurement unit messures an amperage above the set threshold. 

Here are scripts for two units:

 - Power messurement - *StromMessen.ino*
 - Individual control of LED strip *ColorProgramm.ino*

*Schreibtisch.ino* combines both scripts and is used in production!

 ## Usage

 This code is used in an personal project including:
  - ELEGOO MEGA 2560
  - Inductive loading station
  - Power messurement unit
  - LED strip with individual control

 ### Project preview

![Preview](Preview.jpg)
