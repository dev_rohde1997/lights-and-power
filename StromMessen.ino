
#define V_IN A0

//New Volt Output
#define VCC2 5
#define GND2 2

float threshold = 515.0;

void setup()
{
  Serial.begin(9600);
  Serial.println("Starting Strom Measure");

  //Pin Mode
  pinMode(VCC2, OUTPUT);
  digitalWrite(VCC2, HIGH);

  pinMode(GND2, OUTPUT);
  digitalWrite(GND2, LOW);
  
}


void loop() {
  
  float value = analogRead(V_IN);
  Serial.print("value -> ");
  Serial.println(value);
  if (value > threshold) {
    Serial.println("Strom fließt");

  } else {
    Serial.println("Nichts Strom");
  }

  delay(500);
}
