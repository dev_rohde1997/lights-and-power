#include <FastLED.h>


#define NUM_LEDS 60
#define PIN 7
#define V_IN A0


//New Volt Output
#define VCC2 5
#define GND2 2

CRGB leds[NUM_LEDS];

float threshold = 513.0;


void setup()
{
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, PIN>(leds, NUM_LEDS);
  //turnOff();
  Serial.println("Starting...");

  //Pin Mode
  pinMode(VCC2, OUTPUT);
  digitalWrite(VCC2, HIGH);

  pinMode(GND2, OUTPUT);
  digitalWrite(GND2, LOW);

  
}




void setLed(int num, int r, int g, int b, int distance) {
    num = calcPos(num, distance);
    if (num > NUM_LEDS || num < 0) {
      return;
    }
    leds[num].r = r; 
    leds[num].g = g; 
    leds[num].b = b; 
}

void setColorWheel() {
  
  memset(leds, 0, NUM_LEDS * 3);
  for (int i = 0; i < NUM_LEDS; i++) {
    setColorWheelLed(i);
  }
  FastLED.show();
}

void turnOff() {
  for (int i = 0; i < NUM_LEDS; i++) {
    setColor(i, 0, 0, 0);
  }
  FastLED.show();
}


void setColorWheelLed(int num) {
  int red = 0;
  int blue = 0;
  int green = 0;
  if (num < 20) {
    red = 255 - (14 * num);
    blue = 0;
    green = 14 * num;
  } else if (num < 40) {
    red = 0;
    blue = 14 * (num - 20);
    green = 255 - (14 * (num - 20));
  } else if (num < 60) {
    red = 15 * (num - 40);
    blue = 255 - (14 * (num - 40));;
    green = 0;

  }

  setColor(num, red, green, blue);

  
}

void setColor(int num, int r, int g, int b) {
    if (num > NUM_LEDS || num < 0) {
      return;
    }
    if (r > 255) r = 255;
    if (b > 255) b = 255;
    if (g > 255) g = 255;

    if (r < 0) r = 0;
    if (b < 0) b = 0;
    if (g < 0) g = 0;
    
    leds[num].r = r; 
    leds[num].g = g; 
    leds[num].b = b; 
}

int calcPos(int i, int distance) {
    int j = i + distance ;
    if (j >= NUM_LEDS) {
      j = j % NUM_LEDS;
    }
    return j;
}

boolean readPower() {
  float value = analogRead(V_IN);
  //Serial.print("value -> ");
  //Serial.println(value);
    
  if (value > threshold) {
    //Serial.println("Strom fließt");
    return true;
  } else {
    //Serial.println("Nichts Strom");
    return false;
  }
}


void loading() {
  const int distance = NUM_LEDS / 2;
  // one at a time
  while(true) {
    for(int i = 7 ; i < NUM_LEDS + 8; i++ ) {
      memset(leds, 0, NUM_LEDS * 3);
  
      //1
      setLed(i, 255, 0, 0, 0);
      setLed(i+1, 255, 45, 0, 0);
      setLed(i+2, 255, 150, 0, 0);
      setLed(i+3, 150, 255, 0, 0);
      setLed(i+4, 0, 255, 0, 0);
      setLed(i+5, 0, 255, 150, 0);
      setLed(i+6, 0, 0, 255, 0);
      setLed(i+7, 255, 0, 255, 0);
  
    
      setLed(i, 255, 0, 0, 0);
      setLed(i-1, 255, 45, 0, 0);
      setLed(i-2, 255, 150, 0, 0);
      setLed(i-3, 150, 255, 0, 0);
      setLed(i-4, 0, 255, 0, 0);
      setLed(i-5, 0, 255, 150, 0);
      setLed(i-6, 0, 0, 255, 0);
      setLed(i-7, 255, 0, 255, 0);
  
      
  
      setLed(i, 255, 0, 0, distance);
      setLed(i-1, 255, 45, 0, distance);
      setLed(i-2, 255, 150, 0, distance);
      setLed(i-3, 150, 255, 0, distance);
      setLed(i-4, 0, 255, 0, distance);
      setLed(i-5, 0, 255, 150, distance);
      setLed(i-6, 0, 0, 255, distance);
      setLed(i-7, 255, 0, 255, distance);
  
      setLed(i, 255, 0, 0, distance);
      setLed(i+1, 255, 45, 0, distance);
      setLed(i+2, 255, 150, 0, distance);
      setLed(i+3, 150, 255, 0, distance);
      setLed(i+4, 0, 255, 0, distance);
      setLed(i+5, 0, 255, 150, distance);
      setLed(i+6, 0, 0, 255, distance);
      setLed(i+7, 255, 0, 255, distance);
      
      FastLED.show();
      delay(80);

      //Cancel condition -> if loading got canceled
      if (analogRead(V_IN) < 502) {
        //Serial.println(analogRead(V_IN));
        setColorWheel();
        delay(10000);
        return;
      }
    }
  }
  

}

void loop() {

  
  
  
  if (readPower()) {
    loading();
  } else {
    turnOff();
  }

   

   
  
  
}
